
'use strict';

var SerialPort   = require("serialport").SerialPort;

var Arduino = module.exports = {
  port: '/dev/ttyACM0',
  enabled: false,
  isOpen: false,
  serialport: undefined,
  
  init: function () {
    if (Arduino.enabled) {
      Arduino.serialport = new SerialPort(Arduino.port, {
        baudrate: 57600
      });
      
      Arduino.serialport.on('open', function (error) {
        if (error) {
          console.log('Error: failed to open serial port:', Arduino.port);
        } else {
          Arduino.isOpen = true;
          console.log('Open');
          
          Arduino.serialport.on('data', Arduino.data);
        }
      });
    } else {
      console.log('Error: serial data is disabled.');
    }
  },
  
  rotate: function (data) {
    if (Arduino.isOpen) {
      var message = ['A', Math.round(data.angle * 100) / 100, '\n'].join('');
      console.log(message);
      
      Arduino.serialport.write(message, function (error, results) {
        if (error) {
          console.error(error);
        }
      });
    }
  },
  
  data: function (data) {
    //console.log(data.toString('utf-8'));
  },
  
  on: function (event, callback) {
    if (Arduino.events === undefined) {
      Arduino.events = {};
    }
    if (Arduino.events[event] === undefined) {
      Arduino.events[event] = [];
    }
    Arduino.events[event].push(callback);
  },
  
  fire: function (event, data) {
    if (Arduino.events !== undefined && Arduino.events[event] !== undefined) {
      for (var n = 0; n < Arduino.events[event].length; n++) {
        Arduino.events[event][n](data);
      }
    }
  }
};
