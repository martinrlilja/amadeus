
CC = gcc
CFLAGS = -g -std=c99
CLIBS = -lm -lasound
CFILES = main.o audio.o array.o

all: amadeus

amadeus: $(CFILES)
	$(CC) $(CFLAGS) main.o audio.o array.o $(CLIBS) -o amadeus

clean:
	rm -f *.o amadeus
