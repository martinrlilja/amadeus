
import sys, random

def interpolate (a, b, x):
    return a * (1 - x) + b * x

def generateNoise (width):
    noise = []
    for i in range(width):
        noise.append(0)
    
    h = .5
    r = 1
    w = width
    
    noise[0] = random.random() * 2. - 1.
    noise[width - 1] = random.random() * 2. - 1.
    
    while w > 1:
        for x in range(width / w):
            i = x * w + w / 2 - 1
            noise[i] += (random.random() * 2. - 1.) * r
            
            i_s = i - w / 2
            for x_i in range(i_s + 1, i):
                noise[x_i] = interpolate(noise[i_s], noise[i], float(x_i - i_s) / float(i - i_s))
            
            if x == (width / w - 1):
                i = width - 1
                i_s = i - w / 2
                for x_i in range(i_s + 1, i):
                    noise[x_i] = interpolate(noise[i_s], noise[i], float(x_i - i_s) / float(i - i_s))
        
        w /= 2
        r *= h
    
    return noise

def addDivide (a, b):
    is_set = 0
    max_x = 0
    max_y = 0
    
    for shift in range(-32, 32):
        sum = -1
        for i in range(max(0, shift), min(len(a) + shift, len(a))):
            value = abs(a[i] + b[i - shift])
            if value > sum:
                sum = value
        
        if sum > max_y:
            max_x = shift
            max_y = sum
            
            is_set = True
    
    if is_set:
        return max_x
    else:
        return len(a) + 1

sys.stdout.write("\rGenerating noise %3.2f%%"% 0)
sys.stdout.flush()

noises = []
for i in range(16):
    noises.append(generateNoise(8192))
    
    sys.stdout.write("\rGenerating noise %3.2f%%"% (i / 64. * 100.))
    sys.stdout.flush()

sys.stdout.write("\rGenerating noise %3.2f%%"% 100)
sys.stdout.flush()
print('\n')

for x in range(-64, 64):
    print('==================')
    print('')
    
    print('Offset: %4i'% x)
    sys.stdout.write("\rSampling %3.2f%%"% 0)
    sys.stdout.flush()
    
    fails = 0
    off_by = 0
    results = []
    
    for s in range(len(noises)):
        noise = noises[s]
        noise_a = noise[2048:6144]
        noise_b = noise[(2048+x):(6144+x)]
        
        res = addDivide(noise_a, noise_b)
        
        if res == len(noise_a) + 1:
            ++fails
        else:
            off_by = abs(res - x)
            results.append(res)
        
        sys.stdout.write("\rSampling %3.2f%%"% (s / float(len(noises)) * 100.))
        sys.stdout.flush()
    
    sys.stdout.write("\rSampling %3.2f%%"% 100)
    sys.stdout.flush()
    print('\n')
    
    print('Fails: %i'% fails)
    print('Off by: %3f'% (off_by / float(len(noises))))
