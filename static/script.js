
var connection = {
  init: function () {
    var self = this;
    self.socket = io.connect("/");
    
    self.socket.on("connect", function () {
      $("#status-webserver").addClass("up").removeClass("down").text("Up");
    });
    self.socket.on("disconnect", function () {
      $("#status-webserver").addClass("down").removeClass("up").text("Down");
    });
    
    self.socket.on("data", function (data) {
      var data = data.data;
      //console.log(data);
      
      render.graphSpeakers(data,
        document.getElementById("microphones-graph").getContext("2d"));
    });
    
    self.socket.on("settings", function (data) {
      $("#settings-speedofsound").text(data.sound_speed);
      $("#settings-microphonedistance").text(data.microphone_distance);
    });
    
    self.socket.on("angle", function (data) {
      $("#sound-angle").text(data.angle);
      $("#sound-ds").text(data.distance);
      $("#sound-intensity").text(data.intensity);
    });
    
    self.socket.on("environment", function (data) {
      $("#environment-noise").text(data.environment.noise.toString().substr(0, 5));
      $("#environment-threshold").text(data.environment.threshold.toString().substr(0, 5));
    });
  },
};

connection.init();

var render = {
  graphSpeakers: function (data, context) {
    context.clearRect(0, 0, 500, 300);
    
    // #CC1816
    render.graph(data, context, "rgba(204, 24, 22, .75)", 0);
    // #161ACC
    render.graph(data, context, "rgba(22, 26, 204, .75)", 1);
    //render.graph1d(data, context, "rgba(204, 24, 22, .75)");
  },
  
  graph: function (data, context, colour, n0) {
    context.strokeStyle = colour;
    context.beginPath();
    var func = context.lineTo;
    for (var n = 0; n < data.length; n++) {
      if (n === n0) {
        func = context.moveTo;
      }
      func.call(context, n * (500 / data.length), (-data[n][n0]) * 2. + 150);
      func = context.lineTo;
    }
    context.stroke();
  },
  
  graph1d: function (data, context, colour) {
    context.strokeStyle = colour;
    context.beginPath();
    var func = context.lineTo;
    for (var n = 0; n < data.length; n++) {
      if (n === 0) {
        func = context.moveTo;
      }
      func.call(context, n * (500 / data.length), (-data[n]) * 300 + 300);
      func = context.lineTo;
    }
    context.stroke();
  }
};
