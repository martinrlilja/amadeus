
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>

#include "array.h"
#include "types.h"

DoubleArray *da_create (unsigned int reserved) {
  DoubleArray *da = malloc(sizeof(DoubleArray));
  da->num = 0;
  da->size = reserved;
  
  da->data = calloc(sizeof(double), reserved);
  
  return da;
}

void da_clear (DoubleArray *da) {
  da->num = 0;
}

int da_add_element (DoubleArray *da, double v) {
  if (da->num >= da->size) {
    if (da_resize(da, da->num + 1) < 0) {
      return -1;
    }
  }
  
  da->data[da->num] = v;
  da->num++;
  return 0;
}

double da_get_element (DoubleArray *da, unsigned int n) {
  return da->data[n];
}

double *da_get_data (DoubleArray *da) {
  return da->data;
}

void da_set_element (DoubleArray *da, unsigned int n, double v) {
  if (n >= da->size) {
    da_resize(da, n + 1);
  }
  da->data[n] = v;
  
  if (n >= da->num) {
    da->num = n + 1;
  }
}

void da_increment_element (DoubleArray *da, unsigned int n, double v) {
  double buffer = da_get_element(da, n);
  da_set_element(da, n, v + buffer);
}

int da_copy (DoubleArray *a, DoubleArray *b) {
  int i;
  
  if (a->num > b->size) {
    if (da_resize(b, a->num) < 0) {
      return -1;
    }
  }
  
  b->num = 0;
  for (i = 0; i < a->num; i++) {
    da_add_element(b, a->data[i]);
  }
}

void da_free (DoubleArray *da) {
  free(da->data);
  free(da);
}

int da_resize (DoubleArray *da, unsigned int size) {
  if (size != da->size) {
    double *data = realloc(da->data, sizeof(double) * size);
    if (!data) {
      return -1;
    } else {
      da->data = data;
      return 0;
    }
  } else {
    return 0;
  }
}

double da_gaussian_function (double x, double size) {
  return (1. / (sqrt(2. * PI * pow(size, 2)))) * exp(-(pow(x, 2) / (2. * pow(size, 2))));
}

void da_gaussian_blur (DoubleArray *da, double size) {
  unsigned int i, j;
  double *buffer = malloc(sizeof(double) * da->num),
      numerator,
      denominator,
      weight;
  
  for (i = 0; i < da->num; i++) {
    buffer[i] = da_get_element(da, i);
  }
  
  for (i = 0; i < da->num; i++) {
    numerator = 0;
    denominator = 0;
    for (j = fmax(0, i - size); j < fmin(da->num, i + size); j++) {
      weight = da_gaussian_function(j - i, size);
      numerator += weight * buffer[j];
      denominator += weight;
    }
    da->data[i] = numerator / denominator;
  }
  
  free(buffer);
}

long da_signal_shift (DoubleArray *a, DoubleArray *b, long lower, long upper) {
  long i, j, max_i;
  int is_set = 0;
  double max_y = 0,
         max_buffer, buffer;
  
  
  for (i = lower; i < upper; i++) {
    max_buffer = 0;
    
    for (j = fmax(0, i); j < fmin(a->num, a->num + i); j++) {
      buffer = fabs(da_get_element(a, j) + da_get_element(b, j - i));
      if (buffer - max_buffer > EPSILON) {
        max_buffer = buffer;
      }
    }
    
    if (max_buffer - max_y > EPSILON) {
      max_i = i;
      max_y = max_buffer;
      
      is_set = 1;
    }
  }
  
  if (is_set) {
    return max_i;
  } else {
    return lower - 1;
  }
}

// http://rosettacode.org/wiki/Fast_Fourier_transform#C
DoubleArray *da_ditfft2 (double complex *buf, double complex *out, unsigned long n, unsigned long step) {
  int i;
  double complex t;
  
  if (step < n) {
    da_ditfft2(out, buf, n, step * 2);
    da_ditfft2(out + step, buf + step, n, step * 2);
    
    for (i = 0; i < n; i += 2 * step) {
      t = cexp(-I * PI * i / n) * out[i + step];
      buf[i / 2]     = out[i] + t;
      buf[(i + n) / 2] = out[i] - t;
    }
  }
}

DoubleArray *da_fft (DoubleArray *da) {
  unsigned long i;
  double complex *buf,
                 *out;
  DoubleArray *output;
  
  buf = malloc(da->num * sizeof(double complex));
  out = malloc(da->num * sizeof(double complex));
  
  output = da_create(da->num);
  
  for (i = 0; i < da->num; i++) {
    buf[i] = out[i] = (double complex)da_get_element(da, i);
  }
  
  da_ditfft2(buf, out, da->num, 1);
  
  for (i = 0; i < da->num / 2; i++) {
    da_add_element(output, abs(creal(buf[i])));
  }
  
  free(out);
  free(buf);
  
  return output;
}
