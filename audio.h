
#ifndef AUDIO_H
#define AUDIO_H

#include <stdlib.h>
#include <inttypes.h>
#include <alsa/asoundlib.h>

#define AUDIO_NUM_DEVIATION_SAMPLES    100
#define AUDIO_NUM_DETECTED_FREQUENCIES 10
#define AUDIO_WRITE_FREQUENCIES // Om den verkligen ska skriva ut frekvenserna till STDOUT.

#include "types.h"
#include "array.h"

struct _soundData {
  DoubleArray *waveform,
              *frequency,
              *average,
             **frequencySamples;
  double deviationDetectedFrequencies;
  unsigned int *detectedFrequencies,
                numDetectedFrequencies;
};

typedef struct _audioInterface {
    snd_pcm_t *handle;
    
    struct _soundData left,
                      right;
    
    double microphoneDistance,
           soundSpeed;
    
    unsigned int channels,
                 bitrate,
                 sampleRate;
    
    unsigned long numberOfAverageSamples;
} AudioInterface;

int audio_get_capture_interface (AudioInterface*);
int audio_read (AudioInterface*);
int audio_analyse (AudioInterface*);
int audio_generate (AudioInterface*);
int audio_close_capture_interface (AudioInterface*);

void audio_write_waveform (AudioInterface*);
void audio_write_frequency (AudioInterface*);
void audio_write_data (DoubleArray*, DoubleArray*);
void audio_filter (AudioInterface*);
void audio_calculate_average (AudioInterface*);
void audio_push_frequency_sample (AudioInterface*);
void audio_calculate_detected_average (AudioInterface*);
void audio_push_detected_frequency (AudioInterface*, unsigned int, unsigned int);
void audio_prepare_channel (AudioInterface*, struct _soundData*);

#endif
