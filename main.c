
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "audio.h"
#include "array.h"

int main (int argc, char **argv) {
    int exit = 0,
        i = 0;
    
    setvbuf(stdout, NULL, _IOLBF, 0);
    
    AudioInterface audio;
    if (audio_get_capture_interface(&audio) < 0) {
        fprintf(stderr, "An error accoured, exiting...\n");
        return -1;
    }
    
    audio.soundSpeed = 343.2;              // Ljud färdas i circa 343,2 m/s.
    audio.microphoneDistance = 11. / 100.; // 11 cm mellan mikrofonerna.
    
    // Skriv ut JSON data till STDOUT.
    printf("{\"sound_speed\":%f,\"microphone_distance\":%f}\n",
        audio.soundSpeed, audio.microphoneDistance);
    
    // Kör huvudloopen.
    while (!exit) {
        audio_read(&audio);
        audio_analyse(&audio);
    }
    
    audio_close_capture_interface(&audio);
    return 0;
}
