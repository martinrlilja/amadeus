
var app   = require("express")(),
  server  = require("http").createServer(app),
  io      = require("socket.io").listen(server, { log: false }),
  arduino = require("./arduino"),
  amadeus = require("./amadeus-pipe")();

var clients = [];

server.listen(8080);
arduino.init();

app.get("/", function (req, res) {
  res.sendfile(__dirname + "/static/index.html");
});

app.get("/script.js", function (req, res) {
  res.sendfile(__dirname + "/static/script.js");
});

app.get("/jquery.js", function (req, res) {
  res.sendfile(__dirname + "/static/jquery.js");
});

app.get("/style.css", function (req, res) {
  res.sendfile(__dirname + "/static/style.css");
});

arduino.on('open', function (data) {
  for (var n = 0; n < clients.length; n++) {
    clients[n].emit("settings", data);
  }
});

amadeus.on("settings", function (data) {
  for (var n = 0; n < clients.length; n++) {
    clients[n].emit("settings", data);
  }
});

amadeus.on("data", function (data) {
  for (var n = 0; n < clients.length; n++) {
    clients[n].emit("data", data);
  }
});

amadeus.on("angle", function (data) {
  for (var n = 0; n < clients.length; n++) {
    clients[n].emit("angle", data);
  }
  arduino.rotate(data);
});

amadeus.on("environment", function (data) {
  for (var n = 0; n < clients.length; n++) {
    clients[n].emit("environment", data);
  }
});

io.sockets.on("connection", function (socket) {
  clients.push(socket);
  socket.emit("settings", amadeus.settings);
  
  socket.on("stepper", function (data) {
    console.log(data);
  });
  
  socket.on("error", function (data) {
    console.log(data);
  });
});
