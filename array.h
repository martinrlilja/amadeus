
#ifndef ARRAY_H
#define ARRAY_H

#include <stdlib.h>

typedef struct _doubleArray {
  unsigned int num,
               size;
  double *data;
} DoubleArray;
DoubleArray *da_create (unsigned int);

int da_add_element (DoubleArray *, double);
int da_copy (DoubleArray *, DoubleArray *);
int da_resize (DoubleArray *, unsigned int);

double da_get_element (DoubleArray *, unsigned int);
double *da_get_data (DoubleArray *);
double da_gaussian_function (double, double);

long da_signal_shift (DoubleArray *, DoubleArray *, long, long);

void da_gaussian_blur (DoubleArray *, double);
void da_set_element (DoubleArray *, unsigned int, double);
void da_increment_element (DoubleArray *, unsigned int, double);
void da_clear (DoubleArray *);
void da_free (DoubleArray *);

DoubleArray *da_fft (DoubleArray *);

#endif
