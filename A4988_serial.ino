
const byte stepPin      = 12,
           directionPin = 11,
           sleep        = 20;

double stepAngle = 1.8;
int currentStep = 0,
    targetStep  = 0;

void setup () {
  Serial.begin(57600);
  
  pinMode(stepPin,      OUTPUT);
  pinMode(directionPin, OUTPUT);
  
  digitalWrite(directionPin, LOW);
}

void loop () {
  if (Serial.available() > 0) {
    int code = Serial.read();
    
    if (code == 'R') {
      currentStep = currentStep - targetStep;
      targetStep  = 0;
    } else if (code == 'A') {
      double inputAngle = Serial.parseFloat();
      targetStep = round(inputAngle / stepAngle);
    } else if (code == 'G') {
      Serial.println(currentStep * stepAngle);
    } else {
      Serial.println('?');
    }
  }
  
  if (abs(targetStep - currentStep) >= 1) {
    digitalWrite(directionPin, targetStep > currentStep);
    step();
    currentStep += sign(targetStep - currentStep);
  }
}

void step () {
  digitalWrite(stepPin, HIGH);
  delay(sleep / 2);
  digitalWrite(stepPin, LOW);
  delay(sleep / 2);
}

int sign (int d) {
  if (d < 0) {
    return -1;
  } else {
    return 1;
  }
}
