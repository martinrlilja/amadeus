
"use strict"

var spawn = require("child_process").spawn;

exports = module.exports = function () {
  return new Amadeus();
};

function Amadeus () {
  var self = this;
  self.output_buffer = "";
  self.settings = {};
  
  self.child = spawn("./amadeus");
  self.child.stdout.on("data", function (data) {
    self.stdout_on_data(data);
  });
}

Amadeus.prototype.on = function (event, callback) {
  var self = this;
  if (self.events === undefined) {
    self.events = {};
  }
  if (self.events[event] === undefined) {
    self.events[event] = [];
  }
  self.events[event].push(callback);
};

Amadeus.prototype.fire = function (event, data) {
  var self = this;
  if (self.events !== undefined && self.events[event] !== undefined) {
    for (var n = 0; n < self.events[event].length; n++) {
      self.events[event][n](data);
    }
  }
};

Amadeus.prototype.stdout_on_data = function (data) {
  var self = this;
  var buffer = data.toString("utf8"),
    parts  = buffer.split("\n");
  
  if (parts.length > 1) {
    self.output_buffer += parts[0];
    self.data_parser(JSON.parse(self.output_buffer));
    self.output_buffer = "";
    
    for (var i = 1; i < parts.length - 1; i++) {
      self.data_parser(JSON.parse(parts[i]));
    }
    
    if (buffer[buffer.length - 1] === "\n") {
      if (parts[parts.length - 1].length > 1) {
        self.data_parser(JSON.parse(parts[parts.length - 1]));
      }
    } else {
      self.output_buffer += parts[parts.length - 1];
    }
  } else {
    self.output_buffer += parts[0];
  }
};

Amadeus.prototype.data_parser = function (data) {
  var self = this;
  if (data.data) {
    self.fire("data", data);
  } else if (data.angle !== undefined) {
    self.fire("angle", data);
  } else if (data.distance !== undefined) {
    self.fire("distance", data);
  } else if (data.sound_speed !== undefined && data.microphone_distance !== undefined) {
    self.settings = data;
    self.fire("settings", data);
  } else if (data.environment !== undefined) {
    self.fire("environment", data);
  }
};

Amadeus.prototype.kill = function () {
  var self = this;
  self.child.kill('SIGHUP');
};
