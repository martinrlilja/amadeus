
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <math.h>
#include <complex.h>
#include <alsa/asoundlib.h>

#include "audio.h"
#include "types.h"
#include "array.h"

int audio_get_capture_interface (AudioInterface *interface) {
  int error,
    i,
    zero = 0;
  uint16_t point;
  snd_pcm_hw_params_t *params;
  
  interface->bitrate  = 44100;
  interface->sampleRate = 4096;
  interface->channels   = 2;
  
  if ((error = snd_pcm_open(&interface->handle, "default", SND_PCM_STREAM_CAPTURE, 0)) < 0) {
    fprintf(stderr, "Cannot open microphone stream: %s\n", snd_strerror(error));
    return -1;
  }
  if ((error = snd_pcm_hw_params_malloc(&params)) < 0) {
    fprintf(stderr, "Cannot allocate HW parameters: %s\n", snd_strerror(error));
    return -1;
  }
  if ((error = snd_pcm_hw_params_any(interface->handle, params)) < 0) {
    fprintf(stderr, "Cannot initialize HW parameters: %s\n", snd_strerror(error));
    return -1;
  }
  if ((error = snd_pcm_hw_params_set_access(interface->handle, params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    fprintf(stderr, "Cannot set access type: %s\n", snd_strerror(error));
    return -1;
  }
  if ((error = snd_pcm_hw_params_set_format(interface->handle, params,  SND_PCM_FORMAT_FLOAT)) < 0) {
    fprintf(stderr, "Cannot set format: %s\n", snd_strerror(error));
    return -1;
  }
  if ((error = snd_pcm_hw_params_set_rate_near(interface->handle, params, &interface->bitrate, &zero)) < 0) {
    fprintf(stderr, "Cannot set sample rate: %s\n", snd_strerror(error));
    return -1;
  }
  if ((error = snd_pcm_hw_params_set_channels(interface->handle, params, interface->channels)) < 0) {
    fprintf(stderr, "Cannot set channels: %s\n", snd_strerror(error));
    return -1;
  }
  if ((error = snd_pcm_hw_params(interface->handle, params)) < 0) {
    fprintf(stderr, "Cannot set parameters: %s\n", snd_strerror(error));
    return -1;
  }
  
  snd_pcm_hw_params_free(params);
  
  if ((error = snd_pcm_prepare(interface->handle)) < 0) {
    fprintf(stderr, "Cannot prepare device: %s\n", snd_strerror(error));
    return -1;
  }
  
  audio_prepare_channel(interface, &interface->left);
  audio_prepare_channel(interface, &interface->right);
  
  return 0;
}

void audio_prepare_channel (AudioInterface *interface, struct _soundData *channel) {
  channel->waveform = da_create(interface->sampleRate);
  channel->average = da_create(interface->sampleRate / 2);
  
  channel->frequencySamples = calloc(sizeof(DoubleArray*), AUDIO_NUM_DEVIATION_SAMPLES);
  channel->detectedFrequencies = calloc(sizeof(unsigned int), AUDIO_NUM_DETECTED_FREQUENCIES);
  channel->deviationDetectedFrequencies = 0;
  channel->numDetectedFrequencies = 0;
}

int audio_read (AudioInterface *interface) {
  int toRead = interface->sampleRate * interface->channels,
      i, error;
  float *data = malloc(sizeof(float) * toRead);
  
  if ((error = snd_pcm_readi(interface->handle, data, toRead / 2)) != toRead / 2) {
    fprintf(stderr, "Read from audio interface failed (%s)\n", snd_strerror(error));
    return -1;
  }
  
  da_clear(interface->left.waveform);
  da_clear(interface->right.waveform);
  
  for (i = 0; i < toRead; i++) {
    if (i % 2 == 0) {
      da_add_element(interface->left.waveform, (double)data[i]);
    } else {
      da_add_element(interface->right.waveform, (double)data[i]);
    }
  }
  
  free(data);
  return 0;
}

void audio_calculate_average (AudioInterface *interface) {
  double a;
  int i, j, numSamples = 0;
  
  for (i = 0; i < interface->sampleRate / 2; ++i) {
    da_set_element(interface->left.average, i, 0);
    da_set_element(interface->right.average, i, 0);
  }
  
  for (i = 0; i < AUDIO_NUM_DEVIATION_SAMPLES; ++i) {
    numSamples++;
    if (!interface->left.frequencySamples[i] || !interface->right.frequencySamples[i]) {
      break;
    }
    
    for (j = 0; j < interface->sampleRate / 2; ++j) {
      da_increment_element(interface->left.average, j, da_get_element(interface->left.frequencySamples[i], j));
      da_increment_element(interface->right.average, j, da_get_element(interface->right.frequencySamples[i], j));
    }
  }
  
  for (i = 0; i < interface->sampleRate / 2; ++i) {
    a = da_get_element(interface->left.average, i);
    da_set_element(interface->left.average, i, a / (double)(numSamples - 1));
    
    a = da_get_element(interface->right.average, i);
    da_set_element(interface->right.average, i, a / (double)(numSamples - 1));
  }
}

void audio_push_frequency_sample (AudioInterface *interface) {
  int i;
  
  if (interface->left.frequencySamples[AUDIO_NUM_DEVIATION_SAMPLES - 1]) {
    da_free(interface->left.frequencySamples[AUDIO_NUM_DEVIATION_SAMPLES - 1]);
  }
  if (interface->right.frequencySamples[AUDIO_NUM_DEVIATION_SAMPLES - 1]) {
    da_free(interface->right.frequencySamples[AUDIO_NUM_DEVIATION_SAMPLES - 1]);
  }
  
  // From second last element to second first element.
  for (i = AUDIO_NUM_DEVIATION_SAMPLES - 1; i > 0; --i) {
    interface->left.frequencySamples[i]  = interface->left.frequencySamples[i - 1];
    interface->right.frequencySamples[i] = interface->right.frequencySamples[i - 1];
  }
  
  interface->left.frequencySamples[0]  = interface->left.frequency;
  interface->right.frequencySamples[0] = interface->right.frequency;
}

void audio_push_detected_frequency (AudioInterface *interface, unsigned int left, unsigned int right) {
  int i;
  
  if (interface->left.numDetectedFrequencies < AUDIO_NUM_DETECTED_FREQUENCIES) {
    interface->left.numDetectedFrequencies++;
    interface->right.numDetectedFrequencies++;
  }
  
  for (i = interface->left.numDetectedFrequencies - 1; i > 0; --i) {
    interface->left.detectedFrequencies[i]  = interface->left.detectedFrequencies[i - 1];
    interface->right.detectedFrequencies[i] = interface->right.detectedFrequencies[i - 1];
  }
  
  interface->left.detectedFrequencies[0] = left;
  interface->right.detectedFrequencies[0] = right;
}

void audio_calculate_detected_average (AudioInterface *interface) {
  int i;
  double leftAverage  = 0,
         rightAverage = 0,
         leftDeviation = 0,
         rightDeviation = 0;
  
  for (i = 0; i < interface->left.numDetectedFrequencies; ++i) {
    leftAverage += interface->left.detectedFrequencies[i];
    rightAverage += interface->right.detectedFrequencies[i];
  }
  
  leftAverage /= (double)interface->left.numDetectedFrequencies;
  rightAverage /= (double)interface->right.numDetectedFrequencies;
  
  for (i = 0; i < interface->left.numDetectedFrequencies; ++i) {
    leftDeviation += pow(interface->left.detectedFrequencies[i] - leftAverage, 2);
    rightDeviation += pow(interface->right.detectedFrequencies[i] - rightAverage, 2);
  }
  
  interface->left.deviationDetectedFrequencies = sqrt(leftDeviation / (double)interface->left.numDetectedFrequencies) * 3. + leftAverage + 5;
  interface->right.deviationDetectedFrequencies = sqrt(rightDeviation / (double)interface->right.numDetectedFrequencies) * 3. + rightAverage + 5;
}

int audio_generate (AudioInterface *interface) {
  
}

double audio_calculate_distance (double u, double d, double r) {
  return acos(1. - (pow(d, 2.) / (2. * pow(u, 2)))) * .5;
}

int audio_analyse (AudioInterface *interface) {
  double distance = 0,
         angle    = 0,
         leftSum  = 0,
         rightSum = 0;
  int leftIsSet   = 0,
      rightIsSet  = 0,
      i, j;
  
  da_gaussian_blur(interface->left.waveform, 4);
  da_gaussian_blur(interface->right.waveform, 4);
  
  interface->left.frequency  = da_fft(interface->left.waveform);
  interface->right.frequency = da_fft(interface->right.waveform);
  
  if (interface->left.frequencySamples[0]) {
    // There is at least one frequency sample.
    audio_calculate_average(interface);
    
    for (i = 100; i < interface->sampleRate / 2; ++i) {
      leftSum = rightSum = 0;
      for (j = 0; j < AUDIO_NUM_DEVIATION_SAMPLES; ++j) {
        if (!interface->left.frequencySamples[j]) {
          break;
        }
        leftSum  += pow(da_get_element(interface->left.frequencySamples[j], i) - da_get_element(interface->left.average, i), 2);
        rightSum += pow(da_get_element(interface->right.frequencySamples[j], i) - da_get_element(interface->right.average, i), 2);
      }
      
      if (5. * sqrt(leftSum / (double)(j - 1)) < fabs(da_get_element(interface->left.frequency, i)) - da_get_element(interface->left.average, i)) {
        leftIsSet++;
      }
      if (5. * sqrt(rightSum / (double)(j - 1)) < fabs(da_get_element(interface->right.frequency, i)) - da_get_element(interface->right.average, i)) {
        rightIsSet++;
      }
    }
    
    audio_calculate_detected_average(interface);
    
    if (1 && !isnan(interface->left.deviationDetectedFrequencies) && !isnan(interface->right.deviationDetectedFrequencies)) {
      printf("{\"environment\":{\"noise\":%f,\"threshold\":%f}}\n",
        (double)(leftIsSet + rightIsSet) / 2.,
        (double)(interface->left.deviationDetectedFrequencies + interface->right.deviationDetectedFrequencies) / 2.);
    }
    
    if (leftIsSet > interface->left.deviationDetectedFrequencies &&
        rightIsSet > interface->right.deviationDetectedFrequencies) {
#ifdef AUDIO_WRITE_FREQUENCIES
      audio_write_frequency(interface);
#endif
      
      distance = da_signal_shift(interface->left.waveform, interface->right.waveform, -128, 128);
      
      distance /= (double)interface->bitrate;
      distance *= interface->soundSpeed;
      
      printf("{\"distance\":%f,\"intensity\":%f}\n", distance, (double)(leftIsSet + rightIsSet) / 2.);
      
      angle = audio_calculate_distance(
        interface->microphoneDistance / 2.,
        distance,
        interface->microphoneDistance / 2.
      );
      
      if (isnan(angle)) {
        angle = PI / 2.;
      }
      
      if (distance < 0) {
        angle = -angle;
      }
      
      printf("{\"angle\":%f,\"distance\":%f,\"intensity\":%f}\n", angle / PI * 180., distance, (double)(leftIsSet + rightIsSet) / 2.);
    }
    
    audio_push_detected_frequency(interface, leftIsSet, rightIsSet);
  }
  
  audio_push_frequency_sample(interface);
}

void audio_write_waveform (AudioInterface *interface) {
  audio_write_data(interface->left.waveform, interface->right.waveform);
}

void audio_write_frequency (AudioInterface *interface) {
  audio_write_data(interface->left.frequency, interface->right.frequency);
}

void audio_write_data (DoubleArray *left, DoubleArray *right) {
  unsigned int i;
  
  printf("{\"data\":[");
  for (i = 0; i < left->num; i++) {
    if (i != 0) {
      printf(",");
    }
    printf("[%f,%f]", da_get_element(left, i), da_get_element(right, i));
  }
  printf("]}\n");
}

int audio_close_capture_interface (AudioInterface *interface) {
  snd_pcm_close(interface->handle);
  return 0;
}
